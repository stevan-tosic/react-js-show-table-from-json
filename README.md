# README #

This is docker set up for ReactJs task

### What is this repository for? ###

* This docker setup helps to use different versions of Nodejs for task purpose without changing local one

## Project prerequisites
To be able to run project in docker container you will need to have the following libraries installed:
- [docker](https://docs.docker.com/engine/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

### How do I get set up? ###

* **This is linux setup guide**
* run `docker-compose up -d` from repository root
* check next url [http://localhost:3001/](http://localhost:3001/), initial ReactJs page should be shown
* check [http://localhost:3001/article-types.json](http://localhost:3001/article-types.json), JSON data should be shown

**If docker setup has an issue with starting, standalone React Application is totally fine**  
JSON file content can be found on `client/public/article-types.json`  
Very Basic React App is located on `client/` directory

# Task description
* Use libraries free of choice to display JSON data.
* We are using [Ant design](https://ant.design/docs/react/introduce) library. Here is documentation for [Ant Design Tables](https://ant.design/components/table/#header)

## Required
* Implement common Table Search functionality for title text fields (titleDE, titleFR, titleIT, titleEN)
* Implement Sorting of all columns except for column `canAddToOrderOnlyWithArticleType`
* Implement filters for all columns except for next columns (acronym, titleDE, titleFR, titleIT, titleEN)
* Implement Pagination, default number of rows per page should be 5. 
* Implement Possibility to chose number of rows per page (i.e 5, 10, 50)
* Table should have horizontal scroll if wider than container.
* Group columns in groups (Possible with [Ant design tables](https://ant.design/components/table/#components-table-demo-grouping-columns))
  * **General**
    * acronym
    * titleDE
    * titleFR
    * titleIT
    * titleEN
    * group
    * isActive
  * **Settings for all articles**
    * isChoEnabled
    * hasStatistic
    * isReadyForDeals
    * isReadyForBundles
    * isFrontendPage
    * isBundle
    * isContentBoxManageable
    * canHaveZeroPrice
  * **Settings for the creation**
    * defaultShippingStatus
    * defaultBrand
    * defaultDeliveryTime
  * **Stock settings**
    * isStockManagementAllowed
    * allowedWithoutDropshipper
    * canHaveStock
    * moreThanOneDropshipper
    * defaultDropshipper
    * allowedWithDropshipper
  * **Order creation**
    * canCreateExperienceVoucher
    * canCreateBoxVoucher
    * canCreateValueVoucher
    * bundlePlusDifferenceArticle
    * bundleMinusDifferenceArticle
  * **Order Mutation**
    * canAddToOrder
    * addOnlyIfExistsInOrder
    * canAddToOrderOnlyWithArticleType
    * canResendArticlesInOrder

## Bonus Tasks
* Use redux as state management
* Implement Eslint airbnb rules and format code according the rules
* Use styled-components for table component styling
* Implement show/hide from selected columns